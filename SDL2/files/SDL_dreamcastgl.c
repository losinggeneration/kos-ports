#include "../SDL_sysvideo.h"

#if SDL_VIDEO_DRIVER_DREAMCAST

#if HAVE_OPENGL
#include <gl/GL.h>
#endif

int Dreamcast_GL_LoadLibrary(_THIS, const char *path)
{
    _this->gl_config.driver_loaded = 1;
    return 0;
}

void Dreamcast_GL_UnloadLibrary(_THIS)
{
    _this->gl_config.driver_loaded = 0;
    return;
}

void* Dreamcast_GL_GetProcAddress(_THIS, const char *proc)
{
#if HAVE_OPENGL
    return glKosGetProcAddress(proc);
#else
    return NULL;
#endif
}


struct KGLContext {};

SDL_GLContext Dreamcast_GL_CreateContext(_THIS, SDL_Window * window)
{
#if HAVE_OPENGL
    struct KGLContext* content = (struct KGLContext*) malloc(sizeof(KGLContext));
    glKosInit();
    glKosBeginFrame();
    return context;
#else
    return NULL;
#endif
}

int Dreamcast_GL_MakeCurrent(_THIS, SDL_Window * window, SDL_GLContext context)
{
    return 0;
}

int Dreamcast_GL_SetSwapInterval(_THIS, int interval)
{
    return SDL_Unsupported();
}

int Dreamcast_GL_GetSwapInterval(_THIS)
{
    return SDL_Unsupported();
}

void Dreamcast_GL_SwapWindow(_THIS, SDL_Window * window)
{
#if HAVE_OPENGL
    glKosFinishFrame();
    glKosBeginFrame();
#endif
}

void Dreamcast_GL_DeleteContext(_THIS, SDL_GLContext context)
{
#if HAVE_OPENGL
    free(context)
#endif
    return;
}

#endif
