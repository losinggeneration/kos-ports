
#ifndef SDL_DREAMCASTVIDEO_H
#define SDL_DREAMCASTVIDEO_H

typedef struct SDL_VideoData {
    int w;
    int h;
    void* buffer;
} SDL_VideoData;

#endif
