TARGET = libSDL2.a
OBJS = src/SDL_assert.o \
       src/SDL.o \
       src/SDL_error.o \
       src/SDL_hints.o \
       src/SDL_log.o \
       src/audio/SDL_audio.o \
       src/audio/SDL_audiocvt.o \
       src/audio/SDL_audiodev.o \
       src/audio/SDL_audiotypecvt.o \
       src/audio/SDL_mixer.o \
       src/audio/SDL_wave.o \
       src/audio/dummy/SDL_dummyaudio.o \
       src/cpuinfo/SDL_cpuinfo.o \
       src/events/SDL_clipboardevents.o \
       src/events/SDL_dropevents.o \
       src/events/SDL_events.o \
       src/events/SDL_gesture.o \
       src/events/SDL_keyboard.o \
       src/events/SDL_mouse.o \
       src/events/SDL_quit.o \
       src/events/SDL_touch.o \
       src/events/SDL_windowevents.o \
       src/file/SDL_rwops.o \
       src/haptic/SDL_haptic.o \
       src/haptic/dummy/SDL_syshaptic.o \
       src/joystick/SDL_gamecontroller.o \
       src/joystick/SDL_joystick.o \
       src/joystick/dummy/SDL_sysjoystick.o \
       src/loadso/dummy/SDL_sysloadso.o \
       src/power/SDL_power.o \
       src/filesystem/dummy/SDL_sysfilesystem.o \
       src/render/SDL_d3dmath.o \
       src/render/SDL_render.o \
       src/render/SDL_yuv_mmx.o \
       src/render/SDL_yuv_sw.o \
       src/render/software/SDL_blendfillrect.o \
       src/render/software/SDL_blendline.o \
       src/render/software/SDL_blendpoint.o \
       src/render/software/SDL_drawline.o \
       src/render/software/SDL_drawpoint.o \
       src/render/software/SDL_render_sw.o \
       src/render/software/SDL_rotate.o \
       src/stdlib/SDL_getenv.o \
       src/stdlib/SDL_iconv.o \
       src/stdlib/SDL_malloc.o \
       src/stdlib/SDL_qsort.o \
       src/stdlib/SDL_stdlib.o \
       src/stdlib/SDL_string.o \
       src/thread/SDL_thread.o \
       src/thread/dreamcast/SDL_syscond.o \
       src/thread/dreamcast/SDL_sysmutex.o \
       src/thread/dreamcast/SDL_syssem.o \
       src/thread/dreamcast/SDL_systhread.o \
       src/timer/SDL_timer.o \
       src/timer/dreamcast/SDL_systimer.o \
       src/video/SDL_blit_0.o \
       src/video/SDL_blit_1.o \
       src/video/SDL_blit_A.o \
       src/video/SDL_blit_auto.o \
       src/video/SDL_blit.o \
       src/video/SDL_blit_copy.o \
       src/video/SDL_blit_N.o \
       src/video/SDL_blit_slow.o \
       src/video/SDL_bmp.o \
       src/video/SDL_clipboard.o \
       src/video/SDL_egl.o \
       src/video/SDL_fillrect.o \
       src/video/SDL_pixels.o \
       src/video/SDL_rect.o \
       src/video/SDL_RLEaccel.o \
       src/video/SDL_shape.o \
       src/video/SDL_stretch.o \
       src/video/SDL_surface.o \
       src/video/SDL_video.o \
       src/video/dreamcast/SDL_dreamcastvideo.o \
       src/video/dreamcast/SDL_dreamcastevents.o \
       src/video/dreamcast/SDL_dreamcastgl.o

TESTDRAW2_SRCS = test/testdraw2.c
TESTDRAW2_TARGET = testdraw2.elf

defaultall: replacefiles $(OBJS) subdirs linklib buildsamples

buildsamples:
	${KOS_CC} ${KOS_CFLAGS} ${KOS_LDFLAGS} ${KOS_LIBS} $(TESTDRAW2_SRCS) $(OBJS) -o $(TESTDRAW2_TARGET) -Iinclude

replacefiles:
	patch -p2 < ../../patches/0001-enable-dreamcast.patch
	patch -p2 < ../../patches/0002-enable-dreamcast-threading.patch
	patch -p2 < ../../patches/0003-enable-dreamcast-video.patch
	mkdir -p src/timer/dreamcast
	mkdir -p src/thread/dreamcast
	mkdir -p src/video/dreamcast
	cp ../../files/SDL_config_dreamcast.h include/
	cp ../../files/SDL_systimer.c src/timer/dreamcast/
	cp ../../files/SDL_systhread.c src/thread/dreamcast/
	cp ../../files/SDL_systhread_c.h src/thread/dreamcast/
	cp ../../files/SDL_syscond.c src/thread/dreamcast/
	cp ../../files/SDL_sysmutex.c src/thread/dreamcast/
	cp ../../files/SDL_sysmutex_c.h src/thread/dreamcast/
	cp ../../files/SDL_syssem.c src/thread/dreamcast/
	cp ../../files/SDL_dreamcastvideo.c src/video/dreamcast/
	cp ../../files/SDL_dreamcastvideo.h src/video/dreamcast/
	cp ../../files/SDL_dreamcastevents.c src/video/dreamcast/
	cp ../../files/SDL_dreamcastevents_c.h src/video/dreamcast/
	cp ../../files/SDL_dreamcastgl.c src/video/dreamcast/
	cp ../../files/SDL_dreamcastgl_c.h src/video/dreamcast/

KOS_CFLAGS += -I$(KOS_PORTS)/include/libGL -Isrc -Iinclude

include ${KOS_PORTS}/scripts/lib.mk
