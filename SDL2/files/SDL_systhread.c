
#include "../../SDL_internal.h"

#ifdef SDL_THREAD_DREAMCAST

#include "SDL_thread.h"
#include "../SDL_thread_c.h"

#include <kos/thread.h>

static void *thdfunc(void *args)
{
    SDL_RunThread(args);
    return NULL;
}

int SDL_SYS_CreateThread(SDL_Thread *thread, void *args)
{
    thread->handle = thd_create(1, thdfunc, args);
    if (thread->handle == NULL) {
        SDL_SetError("Not enough resources to create thread");
        return -1;
    }
    return 0;
}

void SDL_SYS_SetupThread(const char *name)
{
    return;
}

SDL_threadID SDL_ThreadID(void)
{
    return (Uint32)thd_get_current();
}

void SDL_SYS_WaitThread(SDL_Thread *thread)
{
    thd_join(thread->handle, NULL);
}

void SDL_SYS_DetachThread(SDL_Thread *thread)
{
    thd_detach(thread->handle);
}

void SDL_SYS_KillThread(SDL_Thread *thread)
{
    thd_destroy(thread->handle);
}

int SDL_SYS_SetThreadPriority(SDL_ThreadPriority priority)
{
    int value;
    if (priority == SDL_THREAD_PRIORITY_LOW) {
        value = 0;
    } else if (priority == SDL_THREAD_PRIORITY_HIGH) {
        value = PRIO_MAX;
    } else {
        value = PRIO_DEFAULT;
    }

    return thd_set_prio(thd_by_tid(SDL_ThreadID()), value);
}

#endif
