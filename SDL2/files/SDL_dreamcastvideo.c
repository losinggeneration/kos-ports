
#include "../../SDL_internal.h"

#if SDL_VIDEO_DRIVER_DREAMCAST

/* SDL internals */
#include "../SDL_sysvideo.h"
#include "SDL_syswm.h"

#include <dc/video.h>
#include "SDL_dreamcastvideo.h"
#include "SDL_dreamcastevents_c.h"
#include "SDL_dreamcastgl_c.h"

//Declarations
int Dreamcast_VideoInit(_THIS);
void Dreamcast_VideoQuit(_THIS);
void Dreamcast_GetDisplayModes(_THIS, SDL_VideoDisplay * display);
int Dreamcast_SetDisplayMode(_THIS, SDL_VideoDisplay * display, SDL_DisplayMode * mode);
int Dreamcast_CreateWindow(_THIS, SDL_Window * window);
int Dreamcast_CreateWindowFrom(_THIS, SDL_Window * window, const void *data);
void Dreamcast_SetWindowTitle(_THIS, SDL_Window * window);
void Dreamcast_SetWindowIcon(_THIS, SDL_Window * window, SDL_Surface * icon);
void Dreamcast_SetWindowPosition(_THIS, SDL_Window * window);
void Dreamcast_SetWindowSize(_THIS, SDL_Window * window);
void Dreamcast_ShowWindow(_THIS, SDL_Window * window);
void Dreamcast_HideWindow(_THIS, SDL_Window * window);
void Dreamcast_RaiseWindow(_THIS, SDL_Window * window);
void Dreamcast_MaximizeWindow(_THIS, SDL_Window * window);
void Dreamcast_MinimizeWindow(_THIS, SDL_Window * window);
void Dreamcast_RestoreWindow(_THIS, SDL_Window * window);
void Dreamcast_SetWindowGrab(_THIS, SDL_Window * window, SDL_bool grabbed);
void Dreamcast_DestroyWindow(_THIS, SDL_Window * window);
SDL_bool Dreamcast_GetWindowWMInfo(_THIS, SDL_Window * window, struct SDL_SysWMinfo *info);

static int Dreamcast_Available(void)
{
    return 1;
}

static void Dreamcast_Destroy(SDL_VideoDevice * device)
{
    if (device->driverdata != NULL) {
        device->driverdata = NULL;
    }
}

static SDL_VideoDevice* Dreamcast_Create()
{
    SDL_VideoDevice *device;
    SDL_VideoData* private_data;

    int status;

    /* Check if the Dreamcast could be initialized */
    status = Dreamcast_Available();
    if (status == 0) {
        /* Dreamcast fail */
        return NULL;
    }

    /* Initialize SDL_VideoDevice structure */
    device = (SDL_VideoDevice *) SDL_calloc(1, sizeof(SDL_VideoDevice));
    if (device == NULL) {
        SDL_OutOfMemory();
        return NULL;
    }

    private_data = (SDL_VideoData*) SDL_calloc(1, sizeof(SDL_VideoData));
    if(private_data == NULL) {
        SDL_OutOfMemory();
        return NULL;
    }

    /* Set device free function */
    device->free = Dreamcast_Destroy;
    device->driverdata = private_data;

    device->VideoInit = Dreamcast_VideoInit;
    device->VideoQuit = Dreamcast_VideoQuit;
    device->GetDisplayModes = Dreamcast_GetDisplayModes;
    device->SetDisplayMode = Dreamcast_SetDisplayMode;
    device->CreateWindow = Dreamcast_CreateWindow;
    device->CreateWindowFrom = Dreamcast_CreateWindowFrom;
    device->SetWindowTitle = Dreamcast_SetWindowTitle;
    device->SetWindowIcon = Dreamcast_SetWindowIcon;
    device->SetWindowPosition = Dreamcast_SetWindowPosition;
    device->SetWindowSize = Dreamcast_SetWindowSize;
    device->ShowWindow = Dreamcast_ShowWindow;
    device->HideWindow = Dreamcast_HideWindow;
    device->RaiseWindow = Dreamcast_RaiseWindow;
    device->MaximizeWindow = Dreamcast_MaximizeWindow;
    device->MinimizeWindow = Dreamcast_MinimizeWindow;
    device->RestoreWindow = Dreamcast_RestoreWindow;
    device->SetWindowGrab = Dreamcast_SetWindowGrab;
    device->DestroyWindow = Dreamcast_DestroyWindow;
    device->GetWindowWMInfo = Dreamcast_GetWindowWMInfo;
    device->PumpEvents = Dreamcast_PumpEvents;

    // OpenGL stuff
    device->GL_LoadLibrary = Dreamcast_GL_LoadLibrary;
    device->GL_GetProcAddress = Dreamcast_GL_GetProcAddress;
    device->GL_UnloadLibrary = Dreamcast_GL_UnloadLibrary;
    device->GL_CreateContext = Dreamcast_GL_CreateContext;
    device->GL_MakeCurrent = Dreamcast_GL_MakeCurrent;
    device->GL_SetSwapInterval = Dreamcast_GL_SetSwapInterval;
    device->GL_GetSwapInterval = Dreamcast_GL_GetSwapInterval;
    device->GL_SwapWindow = Dreamcast_GL_SwapWindow;
    device->GL_DeleteContext = Dreamcast_GL_DeleteContext;

    return device;
}

VideoBootStrap Dreamcast_bootstrap = {
    "Dreamcast",
    "Dreamcast Video Driver",
    Dreamcast_Available,
    Dreamcast_Create
};

int Dreamcast_VideoInit(_THIS)
{
    SDL_VideoDisplay display;
    SDL_DisplayMode current_mode;

    SDL_zero(display);
    SDL_zero(current_mode);

    // Default to the lowest supported mode
    current_mode.w = 320;
    current_mode.h = 240;
    current_mode.refresh_rate = 50; // For safety (PAL)
    current_mode.driverdata = NULL;

    //FIXME? SDL 1.x uses RGB655 which doesn't exist...
    current_mode.format = SDL_PIXELFORMAT_RGB565;

    display.desktop_mode = current_mode;
    display.current_mode = current_mode;
    display.driverdata = NULL;

    SDL_AddVideoDisplay(&display);

    return 1;
}

void Dreamcast_VideoQuit(_THIS)
{
    return;
}

void Dreamcast_GetDisplayModes(_THIS, SDL_VideoDisplay * display)
{

    // FIXME: Check these modes work, add 32 bit where possible add the odd 768 ones
    const static SDL_DisplayMode modes[] = {
        { SDL_PIXELFORMAT_RGB555, 320, 240, 50, NULL }, // 15-bit
        { SDL_PIXELFORMAT_RGB565, 320, 240, 50, NULL }, // 16-bit
        { SDL_PIXELFORMAT_RGB555, 320, 240, 60, NULL },
        { SDL_PIXELFORMAT_RGB565, 320, 240, 60, NULL },
        { SDL_PIXELFORMAT_RGB555, 640, 480, 50, NULL }, // 15-bit
        { SDL_PIXELFORMAT_RGB565, 640, 480, 50, NULL }, // 16-bit
        { SDL_PIXELFORMAT_RGB555, 640, 480, 60, NULL },
        { SDL_PIXELFORMAT_RGB565, 640, 480, 60, NULL },
        { SDL_PIXELFORMAT_RGB555, 800, 608, 50, NULL }, // 15-bit
        { SDL_PIXELFORMAT_RGB565, 800, 608, 50, NULL }, // 16-bit
        { SDL_PIXELFORMAT_RGB555, 800, 608, 60, NULL },
        { SDL_PIXELFORMAT_RGB565, 800, 608, 60, NULL }
    };

    const static unsigned int MODE_COUNT = sizeof(modes) / sizeof(SDL_DisplayMode);
    unsigned int i = 0;
    for(; i < MODE_COUNT; ++i) {
        SDL_AddDisplayMode(display, &modes[i]);
    }
}

int Dreamcast_SetDisplayMode(_THIS, SDL_VideoDisplay * display, SDL_DisplayMode * mode)
{
    //FIXME: Need to handle 32bit + OpenGL mode

    int disp_mode = DM_320x240;
    if(mode->w == 640 && mode->h == 480) {
        disp_mode = DM_640x480;
    } else if(mode->w == 800 && mode->h == 608) {
        disp_mode = DM_800x608;
    } else if(mode->w != 320 || mode->h != 240) {
        return SDL_SetError("Unsupported display mode");
    }

    int pixel_mode = PM_RGB565;
    switch(mode->format) {
        case SDL_PIXELFORMAT_RGB555:
            pixel_mode = PM_RGB555;
        break;
        case SDL_PIXELFORMAT_RGB565:
            pixel_mode = PM_RGB565;
        break;
        default:
            return SDL_SetError("Unsupported pixel format");
    }

    vid_set_mode(disp_mode, pixel_mode);

    return 0;
}

int Dreamcast_CreateWindow(_THIS, SDL_Window * window)
{
    return 0;
}

int Dreamcast_CreateWindowFrom(_THIS, SDL_Window * window, const void *data)
{
    return SDL_Unsupported();
}

void Dreamcast_SetWindowTitle(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_SetWindowIcon(_THIS, SDL_Window * window, SDL_Surface * icon)
{
    return;
}

void Dreamcast_SetWindowPosition(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_SetWindowSize(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_ShowWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_HideWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_RaiseWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_MaximizeWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_MinimizeWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_RestoreWindow(_THIS, SDL_Window * window)
{
    return;
}

void Dreamcast_SetWindowGrab(_THIS, SDL_Window * window, SDL_bool grabbed)
{
    return;
}

void Dreamcast_DestroyWindow(_THIS, SDL_Window * window)
{
    return;
}

SDL_bool Dreamcast_GetWindowWMInfo(_THIS, SDL_Window * window, struct SDL_SysWMinfo *info)
{
    if (info->version.major <= SDL_MAJOR_VERSION) {
        return SDL_TRUE;
    } else {
        SDL_SetError("application not compiled with SDL %d.%d\n",
                     SDL_MAJOR_VERSION, SDL_MINOR_VERSION);
        return SDL_FALSE;
    }

    /* Failed to get window manager information */
    return SDL_FALSE;
}

#endif
