
#include "../../SDL_internal.h"

#ifdef SDL_THREAD_DREAMCAST

#include "SDL_thread.h"

#include <kos/sem.h>

struct SDL_semaphore
{
    semaphore_t sem;
};

SDL_sem *SDL_CreateSemaphore(Uint32 initial_value)
{
    SDL_sem *rv;

    if((rv = (SDL_sem *)malloc(sizeof(SDL_sem))))
        sem_init(&rv->sem, initial_value);
    else
        SDL_OutOfMemory();

    return rv;
}

/* WARNING:
   You cannot call this function when another thread is using the semaphore.
*/
void SDL_DestroySemaphore(SDL_sem *sem)
{
    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return;
    }

    sem_destroy(&sem->sem);
    free(sem);
}

int SDL_SemWaitTimeout(SDL_sem *sem, Uint32 timeout)
{
    int retval;

    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return -1;
    }

    /* A timeout of 0 is an easy case */
    if(timeout == 0) {
        return SDL_SemTryWait(sem);
    }

    retval = sem_wait_timed(&sem->sem,timeout);
    if(retval==-1) retval= SDL_MUTEX_TIMEDOUT;

    return retval;
}

int SDL_SemTryWait(SDL_sem *sem)
{
    int retval;

    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return -1;
    }

    retval = sem_trywait(&sem->sem);
    if (retval==0) return 0;
    else return SDL_MUTEX_TIMEDOUT;

    return retval;
}

int SDL_SemWait(SDL_sem *sem)
{
    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return -1;
    }

    sem_wait(&sem->sem);
    return 0;
}

Uint32 SDL_SemValue(SDL_sem *sem)
{
    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return -1;
    }

    return sem_count(&sem->sem);
}

int SDL_SemPost(SDL_sem *sem)
{
    if(!sem) {
        SDL_SetError("Passed a NULL semaphore");
        return -1;
    }

    sem_signal(&sem->sem);
    return 0;
}

#endif
